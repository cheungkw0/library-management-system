package com.library_management_system;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;

public class Library {
    List<Book> books = new ArrayList<Book>();

    public void addBook(Book book) {
        this.books.add(book);
    }

    public void removeBook(String isbn) {
        Optional<Book> targetBook = books.stream().filter(book -> book.isbn.equals(isbn)).findAny();
        if (targetBook.isPresent()) {
            this.books.remove(targetBook.get());
        }
    }

    public List<Book> getBooksPublishedAfterYear(int year) {
        return books.stream().filter(book -> book.publishedYear > year).collect(Collectors.toList());
    }

    public List<String> getAuthorsOfBooksPublishedBeforeYear(int year) {
        return books.stream().filter(book -> book.publishedYear < year)
                .map(book -> book.author)
                .distinct()
                .collect(Collectors.toList());
    }

}
